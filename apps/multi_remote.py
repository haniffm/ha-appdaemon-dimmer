import appdaemon.plugins.hass.hassapi as hass
from datetime import datetime
from enum import Enum


class Button(Enum):
    TOGGLE = 0
    TURN_ON = 1
    TURN_OFF = 2
    INC_BRIGHTNESS = 3
    DEC_BRIGHTNESS = 4
    INC_COLOR = 5
    DEC_COLOR = 6
    INC_BRIGHTNESS_LONGPRESS_START = 7
    INC_BRIGHTNESS_LONGPRESS_STOP = 8
    DEC_BRIGHTNESS_LONGPRESS_START = 9
    DEC_BRIGHTNESS_LONGPRESS_STOP = 10
    INC_COLOR_LONGPRESS_START = 11
    INC_COLOR_LONGPRESS_STOP = 12
    DEC_COLOR_LONGPRESS_START = 13
    DEC_COLOR_LONGPRESS_STOP = 14
    DIRECTIONAL_BRIGHTNESS_LONGPRESS_START = 15
    DIRECTIONAL_BRIGHTNESS_LONGPRESS_STOP = 16


class MultiRemote(hass.Hass):

    def initialize(self):
        self.log("Initializing new MultiRemote...")
        # required

        # configure entities
        entities = self.args["entities"]
        self.light_id = entities["light"]
        self.remote_id = entities["switch"]

        self.event_type = self.args.get("event_type", "deconz_event")

        self.step = self.args.get("step", 10)
        self.delay = self.args.get("delay", 0.3)
        self.default_color_temp = self.args.get("default_color_temp", 300)

        self.MAX_LOOP_COUNTER = 30
        self.state_entity_id = f"appdaemon.{self.light_id.replace('.','_dot_')}_{self.remote_id.replace('.','_dot_')}"
        self.BUTTONS = {}

        if (not self.args.get("buttons")):
            raise ValueError("The 'buttons' dict is not defined! Please define the buttons dict with your apps.yml configuration.")

        for elem in self.args.get("buttons"):
            for k,v in elem.items():
                button_key_name = Button.__dict__.get(k)

                if (button_key_name):
                    self.BUTTONS.update({button_key_name: v})
                else:
                    self.log(f"The button '{k}' does not exists! Could it be misspelled? Supported Buttons are: {Button._member_names_}", level="ERROR")
                    raise ValueError("Buttons configuration error! See previous errors!")

        self.direction = True
        self.set_state(self.state_entity_id, state="on", attributes={"current_event": 0 })

        self.listen_event(self.event_to_state, self.event_type)
        self.listen_state(self.button_state_changed, self.state_entity_id, attribute = "all")

    async def event_to_state(self, event_type, data, kwargs):
        if data.get('id') == self.remote_id:
            await self.set_state(self.state_entity_id, state="on", attributes={"current_event": data.get('event'), "tt": datetime.utcnow()})

    async def get_current_event(self):
        return await self.get_state(self.state_entity_id, attribute="current_event")

    def revert_direction(self):
        self.direction = not self.direction
        return self.direction

    async def button_state_changed(self, entity, attribute, old, new, kwargs):
        current_event = await self.get_current_event()
        if current_event == None:
            return

        if current_event == self.BUTTONS.get(Button.TOGGLE):
            self.log("In TOGGLE")
            await self.call_service("homeassistant/toggle", entity_id=self.light_id)

        elif current_event == self.BUTTONS.get(Button.TURN_OFF):
            self.log("In TURN_OFF")
            await self.call_service("homeassistant/turn_off", entity_id=self.light_id)

        elif current_event == self.BUTTONS.get(Button.TURN_ON):
            self.log("In TURN_ON")
            await self.call_service("homeassistant/turn_on", entity_id=self.light_id)

        elif current_event == self.BUTTONS.get(Button.INC_BRIGHTNESS):
            self.log("In INC_BRIGHTNESS")
            await self.change_attr_value_single(self.step, self.light_id, "brightness", 0)

        elif current_event == self.BUTTONS.get(Button.DEC_BRIGHTNESS):
            self.log("In DEC_BRIGHTNESS")
            await self.change_attr_value_single(-self.step, self.light_id, "brightness", 255)

        elif current_event == self.BUTTONS.get(Button.DEC_COLOR):
            self.log("In DEC_COLOR")
            await self.change_attr_value_single(-self.step, self.light_id, "color_temp", self.default_color_temp)

        elif current_event == self.BUTTONS.get(Button.INC_COLOR):
            self.log("In INC_COLOR")
            await self.change_attr_value_single(self.step, self.light_id, "color_temp", self.default_color_temp)

        elif current_event == self.BUTTONS.get(Button.INC_BRIGHTNESS_LONGPRESS_START):
            self.log("In INC_BRIGHTNESS_LONGPRESS_START")
            await self.change_attr_value(current_event, self.step, self.light_id, "brightness", 0)

        elif current_event == self.BUTTONS.get(Button.DEC_BRIGHTNESS_LONGPRESS_START):
            self.log("In DEC_BRIGHTNESS_LONGPRESS_START")
            await self.change_attr_value(current_event, -self.step, self.light_id, "brightness", 255)

        elif current_event == self.BUTTONS.get(Button.DEC_COLOR_LONGPRESS_START):
            self.log("In DEC_COLOR_LONGPRESS_START")
            await self.change_attr_value(current_event, -self.step, self.light_id, "color_temp", self.default_color_temp)

        elif current_event == self.BUTTONS.get(Button.INC_COLOR_LONGPRESS_START):
            self.log("In INC_COLOR_LONGPRESS_START")
            await self.change_attr_value(current_event, self.step, self.light_id, "color_temp", self.default_color_temp)

        # Actions for single buttons
        elif current_event == self.BUTTONS.get(Button.DIRECTIONAL_BRIGHTNESS_LONGPRESS_START):
            self.log("In DIRECTIONAL_BRIGHTNESS_LONGPRESS_START")
            if self.direction:
                await self.change_attr_value(current_event, self.step, self.light_id, "brightness", 0)
            else:
                await self.change_attr_value(current_event, -self.step, self.light_id, "brightness", 255)

        elif current_event == self.BUTTONS.get(Button.DIRECTIONAL_BRIGHTNESS_LONGPRESS_STOP):
            self.log("In DIRECTIONAL_BRIGHTNESS_LONGPRESS_STOP")
            self.revert_direction()

        # Buttons that currently doesn't need to do anything
        elif current_event == self.BUTTONS.get(Button.INC_BRIGHTNESS_LONGPRESS_STOP):
            self.log("In INC_BRIGHTNESS_LONGPRESS_STOP")

        elif current_event == self.BUTTONS.get(Button.DEC_BRIGHTNESS_LONGPRESS_STOP):
            self.log("In DEC_BRIGHTNESS_LONGPRESS_STOP")

        elif current_event == self.BUTTONS.get(Button.DEC_COLOR_LONGPRESS_STOP):
            self.log("In DEC_COLOR_LONGPRESS_STOP")

        elif current_event == self.BUTTONS.get(Button.INC_COLOR_LONGPRESS_STOP):
            self.log("In INC_COLOR_LONGPRESS_STOP")

        # If all fails
        else:
            self.log(f"Unhandled event ({current_event}) triggered.")

    async def change_attr_value(self, current_event, diff, light_id, attr_name, default_value=None):
        cur_value = self.safe_cast(await self.get_state(light_id, attribute=attr_name), int, default_value)
        loop_counter = 0

        while loop_counter < self.MAX_LOOP_COUNTER and await self.get_current_event() == current_event:
            loop_counter += 1

            cur_value += diff
            await self.call_service("homeassistant/turn_on", entity_id=self.light_id, **{attr_name: cur_value})
            await self.sleep(self.delay)

    async def change_attr_value_single(self, diff, light_id, attr_name, default_value=None):
        cur_value = self.safe_cast(await self.get_state(light_id, attribute=attr_name), int, default_value)

        cur_value += diff
        await self.call_service("homeassistant/turn_on", entity_id=self.light_id, **{attr_name: cur_value})

    @staticmethod
    def safe_cast(val, to_type, default=None):
        try:
            return to_type(val)
        except (ValueError, TypeError):
            return default
