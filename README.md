ha-appdaemon-dimmer
-------------------
HomeAssistant does not (currently) have built in support for dimming lights for a battery driven button. So I've created an AppDaemon script for doing that.

## Getting started

- Install AppDaemon in HomeAssistant
- Put the python script where AppDaemon want them (`.../appdaemon/apps/`)
- Add configuration for your lights in `.../appdaemon/apps/apps.yaml` (check the example in `apps.yaml` or below examples)

## Examples
```
---

# For remote control with 1 button and ability to long press like Aqara
example_single_button:
  module: multi_remote
  class: MultiRemote
  entities:
    switch: my_rc_1
    light: light.light_1
  buttons:
    - DIRECTIONAL_BRIGHTNESS_LONGPRESS_START: 1001
    - DIRECTIONAL_BRIGHTNESS_LONGPRESS_STOP: 1003
    - TOGGLE: 1002

# For remote control with 2 buttons and ability to long press.
example_two_button:
  module: multi_remote
  class: MultiRemote
  entities:
    switch: my_rc_2
    light: light.light_2
  buttons:
    - INC_BRIGHTNESS_LONGPRESS_START: 1001
    - INC_BRIGHTNESS: 1002
    - INC_BRIGHTNESS_LONGPRESS_STOP: 1003
    - DEC_BRIGHTNESS_LONGPRESS_START: 2001
    - TURN_OFF: 2002
    - DEC_BRIGHTNESS_LONGPRESS_STOP: 2003

# For remote control with 5 buttons and ability to long press like Ikea RC.
bedroom_light_rc:
  module: multi_remote      # Name of the python script
  class: MultiRemote        # Name of the class in the above script
  event_type: deconz_event  # (Optional) Type of event to fetch the switch state changes (default: deconz_event)
  entities:
    switch: my_rc_5        # The id of the switch
    light: light.light_5   # The id of the light we want control
  step: 25                  # (Optional) The change in brightness/color per seconds (Default 10)
  delay: 0.3                # (Optional) Delay between each change on long press (Default 0.3 seconds)
  buttons:                  # Function to button event mapping
    - TOGGLE: 1002
    - INC_BRIGHTNESS_LONGPRESS_START: 2001
    - INC_BRIGHTNESS: 2002
    - INC_BRIGHTNESS_LONGPRESS_STOP: 2003
    - DEC_BRIGHTNESS_LONGPRESS_START: 3001
    - DEC_BRIGHTNESS: 3002
    - DEC_BRIGHTNESS_LONGPRESS_STOP: 3003
    - DEC_COLOR_LONGPRESS_START: 4001
    - DEC_COLOR: 4002
    - DEC_COLOR_LONGPRESS_STOP: 4003
    - INC_COLOR_LONGPRESS_START: 5001
    - INC_COLOR: 5002
    - INC_COLOR_LONGPRESS_STOP: 5003
```

## Button function mappings
Each remote control button can send different event IDs (single press, double press, triple press, long press). To map each event to a light functionality, we have implemented the following functionalities. You just need to map it to a corresponding event id (usually a number).

| FUNCTION NAME                          | DESCRIPTION                                                                                                        |
|----------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| TOGGLE                                 | Toggle the light on or off                                                                                         |
| TURN_ON                                | Turn on the light                                                                                                  |
| TURN_OFF                               | Turn off the light                                                                                                 |
| INC_BRIGHTNESS                         | Increase the brightness                                                                                            |
| DEC_BRIGHTNESS                         | Decrease the brightness                                                                                            |
| INC_COLOR                              | Increase the color temperature                                                                                     |
| DEC_COLOR                              | Decrease the color temperature                                                                                     |
| INC_BRIGHTNESS_LONGPRESS_START         | Increase the brightness by holding down the button                                                                 |
| INC_BRIGHTNESS_LONGPRESS_STOP          | Stop increasing the brightness by letting button go                                                                |
| DEC_BRIGHTNESS_LONGPRESS_START         | Decrease the brightness by holding down the button                                                                 |
| DEC_BRIGHTNESS_LONGPRESS_STOP          | Stop decreasing the brightness by letting button go                                                                |
| INC_COLOR_LONGPRESS_START              | Increase the color temperature by holding down the button                                                          |
| INC_COLOR_LONGPRESS_STOP               | Stop increasing the color temperature by letting button go                                                         |
| DEC_COLOR_LONGPRESS_START              | Decrease the color temperature by holding down the button                                                          |
| DEC_COLOR_LONGPRESS_STOP               | Stop decreasing the color temperature by letting button go                                                         |
| DIRECTIONAL_BRIGHTNESS_LONGPRESS_START | Start changing brightness by long pressing (direction of the brightness change is reversed on each button release) |
| DIRECTIONAL_BRIGHTNESS_LONGPRESS_STOP  | Stop changing brightness by letting button go                                                                      |
